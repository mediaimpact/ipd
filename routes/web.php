<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front.fullpage');
})->name('inicio');
Route::get('/fullpage', function () {
    return view('front.fullpage');
});
Route::get('/bienvenido', function () {
    return view('front.bienvenido');
})->name('bienvenido');
Route::get('/registro', function () {
    return view('front.registro');
})->name('registro');

Route::group(['middleware' => 'auth'], function () {

	Route::get('/formulario', 'PageController@formulario')->name('formulario');
	Route::post('/create_candidato', 'PageController@create_candidato')->name('create_candidato');
	Route::post('/update_perfil', 'PageController@update_perfil')->name('update_perfil');
	Route::get('/salir', 'PageController@salir')->name('salir');
	Route::get('/gracias', function () {
	    return view('front.gracias');
	})->name('gracias');
	Route::get('/terminos', function () {
	    return view('front.terminos');
	})->name('terminos');
});

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
Route::get('/login', function () {
    return redirect('/registro');
})->name('login');
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
