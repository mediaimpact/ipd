<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidato extends Model
{
    //
    protected $table = 'candidatos';
    protected $primarykey = 'id';
    protected $fillabel = [
    	'nombres','apellidos','telefono','sustento','file','cat_id'
    ];

    public function categoria()
    {
        return $this->belongsTo('App\Categoria');
    }
}