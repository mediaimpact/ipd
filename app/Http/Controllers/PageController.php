<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Categoria;
use App\Candidato;
use App\User;
use Session;
use Redirect;

use Illuminate\Support\Facades\Mail;
use App\Mail\Emialpostulacion;

class PageController extends Controller
{
    //
    public function formulario(){
        $categorias = Categoria::all();
        //dd($tickets);
        return view('front.formulario')
        ->with('categorias',$categorias);
    }

    public function create_candidato(Request $request){
    	//dd($request->nombres);

    	$candidato = new Candidato;

    	$candidato->nombres = $request->nombres;
    	$candidato->apellidos = $request->apellidos;
    	$candidato->telefono = $request->telefono;
    	$candidato->sustento = $request->sustento;
    	$candidato->cat_id = $request->cat_id;

        if($request->file('file'))
        {
            $file = $request->file('file');
            $name = 'candidato'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/file/';
            $file->move($path, $name);
            $candidato->file = $name;
        }

        $candidato->save();


        $categoria = Categoria::find($request->cat_id);

        $clien2 = "Candidato propuesto";
        $user = User::find(\Auth::user()->id);

        Mail::send('emails.candidato', ['nombres'=>$request->nombres,'apellidos'=>$request->apellidos,'telefono'=>$request->telefono,'sustento'=>$request->sustento,'categoria'=>$categoria->descripcion,'unombre'=>$user->name,'uapellido'=>$user->apellido,'email'=>$user->email], function ($message) use ($clien2,$name)
        {
            $message->from('ipd@mediaimpact.pe', 'Nuevo Candidato - IPD');
            $message->to('jgonzalesv@ipd.gob.pe');
            $message->bcc('genixxavier@gmail.com');
            if($name){
                $message->attach("file/".$name);
            }
            $message->subject($clien2);

        });

        
        $to = $user->email;
        $clien3 = "Candidato Enviado";
        Mail::send('emails.emailuser', ['nombres'=>$user->name,'apellidos'=>$user->apellido,'email'=>$user->email,'telefono'=>$user->telefono,'dni'=>$user->dni,'cnombre'=>$request->nombres,'capellido'=>$request->apellidos,'categoria'=>$categoria->descripcion], function ($message) use ($clien3,$to)
        {
            $message->from('ipd@mediaimpact.pe', 'Candidato Enviado - IPD');
            $message->to($to);
            $message->bcc('genixxavier@gmail.com');
 
            $message->subject($clien3);

        });

        //return view('front.gracias');
        return redirect('/gracias');
    }

    public function update_perfil(Request $request){
        $user = User::find(\Auth::user()->id);
        $user->fill($request->all());
        $user->save();

        return back()->with('status', 'exito');
    }

     public function salir(){
        //Desconctamos al usuario
        \Auth::logout();

        //Redireccionamos al inicio de la app con un mensaje
        //return view('front.fullpage');
        return redirect('/');
    }
}
