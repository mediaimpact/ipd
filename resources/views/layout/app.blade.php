<!doctype html>
<html lang="es">
  <head>
    <title>@yield('titulo')</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	<link href="http://allfont.es/allfont.css?fonts=arial-black" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/icons.css') }}">
	<link rel="stylesheet" href="{{ asset('css/menu.css') }}">
	<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
	<link href="{{asset('img/favicon.png')}}" rel="shortcut icon">
    <link rel="icon" href="{{asset('img/favicon.png')}}">
	@yield('css')
  </head>
  <body>
	<header>
	    <div class="container-fluid">
	    	<div class="container">
	    		<div class="row align-items-center">
	    			<div class="col logos">
	    				<a href="{{route('inicio')}}"><img src="{{ asset('img/logo.png') }}" alt=""><img src="{{ asset('img/n-logo.png') }}" class="logo-1" alt=""></a>
	    				<img src="{{ asset('img/logo2.png') }}" alt="">
	    			</div>
	    			<div class="col-1">
	    				<div class="menu">
	    					<a href="javascript:void(0)" id="barmenu"><i class="fas fa-bars"></i></a>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </div>
	</header>
	<div id="showmenu" class="">
		<ul>
			<li><a href="#section0" data-ancla="section0" class="anclasmenu">Presentación</a></li>
			<li><a href="#scene02" data-ancla="scene02" class="anclasmenu">¿Cómo participar?</a></li>
			<li><a href="#scene03" data-ancla="scene03" class="anclasmenu">Valores</a></li>
			<li><a href="#scene07" data-ancla="scene07" class="anclasmenu">Etapas</a></li>
			<li><a href="#scene08" data-ancla="scene08" class="anclasmenu">Jurado</a></li>
			<li><a href="#scene09" data-ancla="scene09" class="anclasmenu">Consejo de Honor</a></li>
			<li><a href="https://facebook.com" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
		</ul>
		<a href="javascript:void(0)" id="cerramenu"><span class="icon-close"></span></a>
	</div>
	<section id="main" class="container-fluid">
		@yield('content')
	</section>
	
	@yield('content_extras')
	<div id="footer" class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-12 ">© 2018 IPD - Desarrollado por <a href="https://www.mediaimpact.pe" target="_blank">Media Impact</a></div>
			</div>
		</div>
	</div>
	<div class="item-mas">
		<ul>
			<li><span class="list-text"><input type="range" name="rango" id="chanceSlider" class="vHorizon" min="0" max="100" step="20"><div class="masletra"> A <span>A</span></div></span><span class="icon-llave ipd-ico"></span></li>
			<li class="active-animacion"><span class="list-text">Reduzca las animaciones y simplifique el contenido para facilitar la comprensión.</span><span class="icon-cabeza ipd-ico"></span></li>
			<!-- <li class="active"><span class="list-text">Active los subtítulos para obtener una experiencia de video mejorada.</span><span class="icon-oido ipd-ico"></span></li> -->
			<li class="active-vista"><span class="list-text">Active los subtítulos para obtener una experiencia de video mejorada.</span><span class="icon-ojo ipd-ico"></span></li>
			<li class="active-inicial"><span class="list-text">Siempre puede volver a activar los controles para mejorar su experiencia en el sitio.</span><span class="icon-candado ipd-ico"></span></li>
		</ul>
	</div>
	<div id="footer-top">
		<div class="triangulo">
			<a href="javascript:void(0)" id="masitem" ><img src="{{asset('img/masitem.png')}}" alt=""></a>
			<a href="javascript:void(0)" id="menositem" ><span class="icon-close"></span></a>
		</div>
	</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>
	<script>
		$(document).ready(function(){
			$("#barmenu").click(function(event) {
                $("body").css({"overflow": "hidden"});
                $("#showmenu").css({"width":"100%"});
                $("#showmenu").css({"opacity":"1"});
            });

            $("#cerramenu").click(function(event) {
                $("body").css({"overflow": "initial"});
                $("#showmenu").css({"width":"0px"});
                $("#showmenu").css({"opacity":"0"});
            });

            $("#footer-top a").click(function(){
            	$("#footer-top a").toggle();
            });
            $("#masitem").click(function(){
            	$(".item-mas").css({"height":"220px"});
            	$("#footer-top a").css({"transition": "all 1s linear"});
                
            });
            $("#menositem").click(function(){
            	$(".item-mas").css({"height":"0px"});
            	$("#footer-top a").css({"transition": "all 1s linear"});

            });

            $( window ).bind('scroll',function() {
			    if ($(window).scrollTop() > 100) {
			        $('header').addClass('bgmenu');
			    } else {
			        $('header').removeClass('bgmenu');
			    }
			});

            $(".active-vista").click(function(){
            	$('body').addClass('ipdvision');
            });
            $(".active-inicial").click(function(){
            	$('body').removeClass('ipdvision');
            	$('body').removeClass('ipdanimation');
            });

            $(".active-animacion").click(function(){
            	$('body').addClass('ipdanimation');
            });
		});
	</script>
    @yield('js')
  </body>
</html>