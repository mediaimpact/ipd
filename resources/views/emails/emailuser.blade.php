<html>
<head></head>
<body>
<h2>Candidato Enviado - IPD</h2>
<h3>Tus datos:</h3>
<p><b>Nombre: </b> {{ $nombres}} {{ $apellidos}}</p>
<p><b>Email: </b> {{ $email}}</p>
<p><b>Teléfono: </b> {{ $telefono}}</p>
<p><b>DNI: </b> {{ $dni}}</p>
<h3>Gracias por proponer al siguiente candidato:</h3>
<p><b>Candidato: </b> {{ $cnombre}} {{ $capellido}}</p>
<p><b>Categoría:</b> {{$categoria}}</p>
</body>
</html>