<html>
<head></head>
<body>
<h2>Nuevo Candidato - IPD</h2>
<p><b>Nombres: </b> {{ $nombres}} {{ $apellidos}}</p>
<p><b>Teléfono: </b> {{ $telefono}}</p>
<p><b>Categoría:</b> {{$categoria}}</p>
<p><b>Sustento de participación: </b> {!! $sustento !!}</p>

<h3>Propuesto por:</h3>
<p><b>Nombre: </b> {{ $unombre}} {{ $uapellido}}</p>
<p><b>Email: </b> {{ $email}}</p>
</body>
</html>