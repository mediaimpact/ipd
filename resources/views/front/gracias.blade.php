@extends('layout.app')
@section('titulo','Gracias')

@section('css')
<link href="{{asset('css/gracias.css')}}" rel="stylesheet">
@endsection

@section('content')
<div class="container" id=pg-gracias>
	<div class="row">
		<div class="col-lg-3 col-md-2"></div>
		<div id="scene01" class="col-lg-6 col-md-8 sceneContainer">
			<section id="frame01" class="contentFrame">
				<div class="wrapper">
					<div class="row lista align-items-center">
						<div class="col">
							<h1>GRACIAS</h1>
							<p>Se ha enviado los datos de tu candidato, mucha gracias por tu participación</p>
						</div>
					</div>
					<div class="row lista text-center">
						<div class="col-md-4"></div>
						<div class="col-md-4 ">
							<a href="{{ route('salir')}}" class="btn btn-block btn-morado">Home</a>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection

@section('js')
<script>
	$(document).ready(function(){
		$('.anclasmenu').click(function(e){               
            var strAncla=$(this).attr('href');

            $("body").css({"overflow": "initial"});
            $("#showmenu").css({"width":"0px"});
            $("#showmenu").css({"opacity":"0"});
            location.href = './'+strAncla;

        });

        $('#chanceSlider').on(' input change', function(){
			var rag = $('#chanceSlider').val();
			var f1 = 2.5 + (rag/70);
			var f16 = 16 + (rag/70);
			var f21 = 21 + (rag/70);
			$('h1').css({
				'font-size': f1+'rem'
			});
			$('p').css({
				'font-size': f21+'px'
			});
			$('.btn-morado').css({
				'font-size': f16+'px'
			});
		});
        
		var hh = $(window).height();
		var sh = $("#scene01").height();
		if(sh < hh){
			$("#scene01").css('height',hh);
		}
		else{
			$("#showmenu ul li a").css('font-size','1.2em');
		}
		$('.anclas').click(function(e){               
            e.preventDefault();
            var strAncla=$(this).attr('href');
            $('body,html').stop(true,true).animate({                
                scrollTop: $(strAncla).offset().top
            },1000);
        });
	});
</script>
@endsection