@extends('layout.app')
@section('titulo','Historial')

@section('css')
<link href="{{asset('css/jquery.fullPage.css')}}" rel="stylesheet">
<link href="{{asset('css/home.css')}}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div id="scene01" class="col sceneContainer">
			<section id="frame01" class="contentFrame">
				<div class="wrapper">
					<h2 class="movieWrpper">
						<p class="text1">Primer Premio Nacional a los <img src="{{asset('img/valores.png')}}" alt=""></p>
						<video class="movieMask" autoplay loop alt="MOVE">
							<source src="{{asset('img/movie.mp4')}}">
						</video>
						<p class="text2">Los <span>valores</span> merecen ser premiados</p>
					</h2>
				</div>
				<a href="#scene02" data-ancla="scene02" class="anclas"><i class="fas fa-arrow-down"></i></a>
			</section>
		</div>
	</div>
</div>
<div id="scene02" class="row sceneContainer">
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8"><img src="{{asset('img/reconocenos.jpg')}}" alt=""></div>
			<div class="col-12 texto-top">
				<h2>Reconocemos los valores que practican los deportistas de todo el país</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 texto-reconocenos">
				<p>El premio Nacional a los Valores Deportivos es desarrollado por el Instituto Peruano de Deporte y la Comisión Nacional Contra la Violencia en los Espectáculos Deportivos en un esfuerzo por promover y desarrollar los valores en la práctica deportiva en el país.</p>
				<a href="{{ route('bienvenido')}}" class="btn btn-morado">¿Cómo puedo participar?</a>
				<p class="base">Descargar las bases <a href="#">aquí</a></p>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script src="{{asset('js/jquery.easings.min.js')}}"></script>
<script src="{{asset('js/scrolloverflow.min.js')}}"></script>
<script src="{{asset('js/jquery.fullPage.js')}}"></script>
<script>
	$(document).ready(function(){
		var hh = $(window).height();
		$("#scene01").css('height',hh);
		$("#scene02").css('height',hh);
		$('.anclas').click(function(e){               
            e.preventDefault();
            var strAncla=$(this).attr('href');
            $('body,html').stop(true,true).animate({                
                scrollTop: $(strAncla).offset().top
            },1000);
        });
	});
</script>
@endsection