@extends('layout.app')
@section('titulo','Formulario')

@section('css')
<link href="{{asset('css/formulario.css')}}" rel="stylesheet">
@endsection

@section('content')
<div id="pg-formulario" class="row bg1">
	<div class="container">
		<div class="row">
			<div id="scene01" class="col sceneContainer">
				<section id="formulario" class="contentFrame">
					<div class="wrapper">
						<div class="row perfil">
							<div class="col-12 text-center">
								<img src="{{asset('img/estrella.png')}}" alt="">
								<img src="{{asset('img/n-estrella.png')}}" alt="" class="nimg">
								<h3>¡Te registraste con éxito <span>{{  \Auth::user()->name }} {{ \Auth::user()->apellido }}</span>!</h3>
							</div>
							@if (session('status'))
								@if(session('status') == "exito")
								<div class="col-12 text-center">
								    <div class="alert alert-success alert-dismissible" role="alert"> 
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
										<strong>Exito</strong> Tus datos han sido actualizados.
									</div>
								</div>
								@endif
							@endif
							<div class="col-lg-3 col-md-2 col-12"></div>
							<div class="col-lg-6 col-md-8 col-12">
								<div class="row align-items-center box-perfil">
									<div class="col-md-4 text-center b-img">
										<img src="{{asset('img/user.png')}}" alt="">
										<img src="{{asset('img/n-user.png')}}" alt="" class="nimg">
									</div>
									<div class="col-md-8 b-datos">
										<a href="javascript:void(0)" class="btn-edit"><i class="fas fa-edit"></i></a>
										<a href="javascript:void(0)" class="btn-close"><i class="fas fa-times"></i></a>
										<h4>Estos son tus datos registrados</h4>
										<table class="table">
											<tr>
												<td>Nombres:</td>
												<td>{{  \Auth::user()->name }} {{ \Auth::user()->apellido }}</td>
											</tr>
											<tr>
												<td>Email:</td>
												<td>{{  \Auth::user()->email }}</td>
											</tr>
											<tr>
												<td>Celular:</td>
												<td>{{  \Auth::user()->telefono }}</td>
											</tr>
											<tr>
												<td>DNI:</td>
												<td>{{  \Auth::user()->dni }}</td>
											</tr>
										</table>
										<form action="{{route('update_perfil')}}"  method="post">
											{{ csrf_field() }}
											<div class="form-group row">
												<div class="col-3">Nombres:</div>
												<div class="col-9">
													<input type="text" name="name" class="form-control" value="{{  \Auth::user()->name }}" required>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-3">Apellidos:</div>
												<div class="col-9">
													<input type="text" name="apellido" class="form-control" value="{{ \Auth::user()->apellido }}" required>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-3">Email:</div>
												<div class="col-9">
													<input type="email" name="email" class="form-control" value="{{  \Auth::user()->email }}" required>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-3">Celular:</div>
												<div class="col-9">
													<input type="text" name="telefono" class="form-control" value="{{  \Auth::user()->telefono }}" required>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-3">DNI:</div>
												<div class="col-9">
													<input type="text" name="dni" class="form-control" value="{{  \Auth::user()->dni }}" required>
												</div>
											</div>
											<button type="submit" class="btn btn-block btn-morado">Actualizar</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
<div class="row bg2">
	<div class="container" id="completar-datos">
		<div class="">
			<div class="col text-center">
				<h4>Ahora puedes llenar los <span>datos de tu candidato</span><br>en el siguiente formulario: </h4>
			</div>
		</div>
		<form action="{{route('create_candidato')}}" id="candidato" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			@if (session('status'))
				@if (session('status') == "exitoc")
				<div class="row">
				    <div class="alert alert-success alert-dismissible" role="alert"> 
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
						<strong>Exito</strong>, los datos de tu candidato fue enviado.
					</div>
				</div>
				@endif
			@endif
			<div class="row item">
				<div class="col-md-4 text-right"><h2>Datos del postulante</h2></div>
				<div class="col-md-8 item-input">
					<div class="form-group">
						<input type="text" class="form-control" name="nombres" id="nombres" placeholder="Nombres" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="apellidos" id="apellidos"  placeholder="Apellidos" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="telefono" id="telefono" placeholder="Teléfono" required>
					</div>
				</div>
			</div>
			<div class="row item">
				<div class="col-md-4 text-right"><h2>Categoría de participación</h2></div>
				<div class="col-md-8 item-input">
					<div class="row">
						<?php $n=0;?>
						@foreach($categorias as $categoria)
						<?php 
							if($n > 1){
								$required = "required";
							}
							else{
								$required = "";
							}
						?>
						<div class="col-6">
							<div class="form-check">
							  <input class="form-check-input" type="radio" name="cat_id" id="categoria{{ $categoria->id}}"  value="{{ $categoria->id }}" {{ $required }}>
							  <label class="form-check-label" for="categoria{{$categoria->id}}">{{ $categoria->descripcion }}</label>
							</div>
						</div>
						<?php $n++; ?>
						@endforeach
					</div>
				</div>
			</div>
			<div class="row item">
				<div class="col-md-4 text-right"><h2>Sustento de participación</h2></div>
				<div class="col-md-8 item-input">
					<div class="form-group">
						<label for="">Cuéntanos más sobre tu candidato y por qué debería ganar</label>
						<textarea name="sustento" id="sustento" class="form-control" cols="20" rows="5" required></textarea>
					</div>
					<div class="form-group files">
						<label for="file" class="file_label">Adjunta tu material sustentatorio</label>
						<span>*Subir archivos menores a 2mb</span>
						<input type="file" class="form-control"  id="file" required>
					</div>
					<div class="form-group terminos">
						<div class="form-check">
						  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" required>
						  <label class="form-check-label" for="defaultCheck1">
							Acepto los <a href="{{route('terminos')}}" target="_blank">Términos y Condiciones</a>
						  </label>
						</div>
					</div>
					<div class="row text-center">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<button type="submit" class="btn btn-block btn-morado">POSTULAR</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="alerta">
	<div class="content-alerta">
		<div class="box-alerta">
			<div>
				<h2><i class="fas fa-spinner fa-spin"></i></h2>
				<h2>Un momento porfavor, estamos procesando sus datos ....</h2>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script>
	$(document).ready(function(){
		if (window.location.hash == '#_=_'){
	        if (history.replaceState) {
	            var cleanHref = window.location.href.split('#')[0];
	            history.replaceState(null, null, cleanHref);

	        } else {
	            window.location.hash = '';
	        }
	    }

	    $('#chanceSlider').on(' input change', function(){
			var rag = $('#chanceSlider').val();
			var f14 = 14 + (rag/70);
			var f16 = 16 + (rag/70);
			var f17 = 17 + (rag/70);
			var f18 = 18 + (rag/70);
			var f20 = 20 + (rag/70);
			var f13 = 13 + (rag/70);
			var f12 = 12 + (rag/70);
			$('body, table td, .terminos label, .btn-morado').css({
				'font-size': f16+'px'
			});
			$('.files span').css({
				'font-size': f14+'px'
			});
			$('.form-check').css({
				'font-size': f17+'px'
			});
			$('h4').css({
				'font-size': f18+'px'
			});
			$('#completar-datos h4, h2,h3').css({
				'font-size': f20+'px'
			});
			$('.btn-facebook, .btn-gmail').css({
				'font-size': f13+'px'
			});
			$('.form-control').css({
				'font-size': f12+'px'
			});
		});

		var hh = $(window).height();
		//$("#scene01").css('height',hh);
		$('.anclasmenu').click(function(e){               
            var strAncla=$(this).attr('href');

            $("body").css({"overflow": "initial"});
            $("#showmenu").css({"width":"0px"});
            $("#showmenu").css({"opacity":"0"});
            location.href = './'+strAncla;

        });

        $("#file").on("change", function (e) {
	        var files = $(this)[0].files;
	        if (files.length >= 2) {
	            $(".file_label").text(files.length + " Files Ready To Upload");
	        } else {
	            var fileName = e.target.value.split("\\").pop();
	            $("#file").attr('name','file');
	            $(".file_label").text(fileName);
	        }
	    });

	    $(".btn-edit").click(function(){
	    	$('.b-datos table').css('display','none');
	    	$('.b-datos form').css('display','block');
	    	$('.btn-edit').hide('fade');
	    	$('.btn-close').show('fade');
	    });
	    $(".btn-close").click(function(){
	    	$('.b-datos table').css('display','block');
	    	$('.b-datos form').css('display','none');
	    	$('.btn-close').hide('fade');
	    	$('.btn-edit').show('fade');
	    });

	    $("#candidato").submit(function(){
	    	$(".alerta").css('display','block');
	    });
	});
</script>
@endsection