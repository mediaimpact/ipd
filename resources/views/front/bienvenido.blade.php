@extends('layout.app')
@section('titulo','¿Cómo puedo participar?')

@section('css')
<link href="{{asset('css/participar.css')}}" rel="stylesheet">
@endsection

@section('content')
<div class="container" id="pg-bienvenido">
	<div class="row">
		<div class="col-lg-3 col-md-2"></div>
		<div id="scene01" class="col-lg-6 col-md-8 sceneContainer">
			<section id="frame01" class="contentFrame">
				<div class="wrapper">
					<div class="row lista align-items-center">
						<div class="col-1">
							<span>1.</span>
						</div>
						<div class="col">
							Identifica a una persona que cumpla con uno de los <span class="text-amarillo">valores</span> deportivos propuestos.
						</div>
					</div>
					<div class="row lista align-items-center">
						<div class="col-1">
							<span>2.</span>
						</div>
						<div class="col">Regístrate e inscribe a tu nominado dándole clic a <b>POSTULAR</b> para agregarlo a la base de datos. Ten en cuenta que el registro solo podrán efectuarlo las personas mayores de edad.</div>
					</div>
					<div class="row lista align-items-center">
						<div class="col-1">
							<span>3.</span>
						</div>
						<div class="col">Llena el formulario y completa el cuadro de texto relatando los motivos que sustenten la participación de tu nominado. Podrás adjuntar fotos, videos u otros documentos que ayuden en la selección de finalistas.</div>
					</div>
					<div class="row lista align-items-center">
						<div class="col-1">
							<span>4.</span>
						</div>
						<div class="col">Acepta los términos y condiciones, envíanos el formulario y ¡listo!</div>
					</div>
					<div class="row lista text-center">
						<div class="col-md-4"></div>
						<div class="col-md-4">
							<a href="{{ route('registro')}}" class="btn btn-block btn-morado">POSTULAR</a>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection

@section('js')
<script>
	$(document).ready(function(){
		$('.anclasmenu').click(function(e){               
            var strAncla=$(this).attr('href');

            $("body").css({"overflow": "initial"});
            $("#showmenu").css({"width":"0px"});
            $("#showmenu").css({"opacity":"0"});
            location.href = './'+strAncla;

        });
        $('#chanceSlider').on(' input change', function(){
			var rag = $('#chanceSlider').val();
			var pp = 16 + (rag/70);
			$('#scene01 #frame01 .wrapper .lista, .btn-morado').css({
				'font-size': pp+'px'
			});
		});
        
		var hh = $(window).height();
		var sh = $("#scene01").height();
		if(sh < hh){
			$("#scene01").css('height',hh);
		}
		else{
			$("#showmenu ul li a").css('font-size','1.2em');
		}

		$( window ).resize(function() {
			var ww = $(window).width();
			var hh = $(window).height();
			var sh = $("#scene01").height();
			if(sh < hh){
				$("#scene01").css('height',hh);
				$("#showmenu ul li a").css('font-size','2em');
			}
			else{
				$("#scene01").css('height','auto');
				$("#showmenu ul li a").css('font-size','1.2em');
			}
		});
		$('.anclas').click(function(e){               
            e.preventDefault();
            var strAncla=$(this).attr('href');
            $('body,html').stop(true,true).animate({                
                scrollTop: $(strAncla).offset().top
            },1000);
        });
	});
</script>
@endsection