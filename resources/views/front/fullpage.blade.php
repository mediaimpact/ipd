@extends('layout.app')
@section('titulo','IPD - Home')

@section('css')
<link href="{{asset('css/jquery.fullPage.css')}}" rel="stylesheet">
<link href="{{asset('js/owl/assets/owl.carousel.min.css')}}" rel="stylesheet">
<link href="{{asset('js/owl/assets/owl.theme.default.min.css')}}" rel="stylesheet">
<link href="{{asset('css/home.css')}}" rel="stylesheet">
@endsection

@section('content')
<div id="preload">
	<div class="cont">
		<div>
			<i class="fas fa-circle-notch fa-3x fa-spin"></i>
		</div>
	</div>
</div>
<div class="row">
	<div id="fullpage">
		<div class="section " id="section0">
			<div class="container">
				<div class="row">
					<div id="scene01" class="col sceneContainer">
						<section id="frame01" class="contentFrame">
							<div class="wrapper">
								<h2 class="movieWrpper">
									<p class="text1">Primer Premio Nacional a los <img src="{{asset('img/valores.png')}}" alt=""> <img src="{{asset('img/n-valores.png')}}" class="imgn" alt=""></p>
									<video class="movieMask" autoplay loop alt="MOVE">
										<source src="{{asset('img/movie.mp4')}}">
										<source src="movie.webm" type='video/webm; codecs="vp8, vorbis"' />
									</video>
									<p class="text2">Los <span>valores</span> merecen ser premiados</p>
								</h2>
							</div>
							<!-- <a href="#scene02" data-ancla="scene02" class="anclas"><i class="fas fa-arrow-down"></i></a> -->
							<a id="moveSectionDown" href="#" class="anclas"><i class="fas fa-arrow-down"></i></a>
						</section>
					</div>
				</div>
			</div>
		</div>
		<div class="section" id="scene02">
			<div id="scene2">
				<div  class="row sceneContainer">
					<div class="container">
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8"><img src="{{asset('img/reconocenos.jpg')}}" alt=""></div>
							<div class="col-12 texto-top">
								<h2>Reconocemos los valores que practican los deportistas de todo el país</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8 texto-reconocenos">
								<p>El Premio Nacional a los Valores Deportivos es desarrollado por el Instituto Peruano de Deporte y la Comisión Nacional Contra la Violencia en los Espectáculos Deportivos en un esfuerzo por promover y desarrollar los valores en la práctica deportiva en el país.</p>
								<a href="{{ route('bienvenido')}}" class="btn btn-morado">¿Cómo puedo participar?</a>
								<p class="base">Descarga las bases <a href="http://mediaimpact.pe" target="_blank">aquí</a></p>
							</div>
						</div>
					</div>
				</div>
				<a id="moveSectionDown" href="#" class="anclas"><i class="fas fa-arrow-down"></i></a>
			</div>
		</div>
		<div class="section" id="scene03">
			<div id="particles-js"></div>
			<div class="row align-items-center">
				<div class="col-lg-7 col-sm-6 col-12 img-p">
					<img src="{{asset('img/imgv1.jpg')}}" class="mv1" alt="">
				</div>
				<div class="col-lg-5 col-sm-6 col-12 texto-v">
					<h2>
						DISCIPLINA
					</h2>
					<p>
						Los candidatos muestran y evidencian orden y dedicación al deporte, programando y cumpliendo los horarios de entrenamiento establecidos satisfactoriamente entre otras consideraciones relacionadas con la práctica deportiva.
					</p>
				</div>
			</div>
		</div>
		<div class="section" id="scene04">
			<div id="particles1"></div>
			<div class="row align-items-center direccion">
				<div class="col-lg-7 col-sm-6 col-12 img-p">
					<img src="{{asset('img/imgv2.jpg')}}" class="mv2" alt="">
				</div>
				<div class="col-lg-5 col-sm-6 col-12 texto-v">
					<h2>
						RESPETO
					</h2>
					<p>Los candidatos presentan un alto nivel de empatía y respeto por otros deportistas, sean o no de su mismo equipo deportivo.</p>
				</div>
			</div>
		</div>
		<div class="section" id="scene05">
			<div id="particles2"></div>
			<div class="row align-items-center">
				<div class="col-lg-7 col-sm-6 col-12 img-p">
					<img src="{{asset('img/imgv3.jpg')}}" class="mv1" alt="">
				</div>
				<div class="col-lg-5 col-sm-6 col-12 texto-v">
					<h2>
						COMPAÑERISMO
					</h2>
					<p>Los candidatos se integran, colaboran y son solidarios con otros integrantes de sus equipos deportivos.</p>
				</div>
			</div>
		</div>
		<div class="section" id="scene06">
			<div id="particles3"></div>
			<div class="row align-items-center direccion">
				<div class="col-lg-7 col-sm-6 col-12 img-p">
					<img src="{{asset('img/imgv4.jpg')}}" class="mv2" alt="">
				</div>
				<div class="col-lg-5 col-sm-6 col-12 texto-v">
					<h2>
						PERSEVERANCIA
					</h2>
					<p>Los candidatos han superado obstáculos, imprevistos y distintas situaciones presentadas en sus trayectoria deportivas.</p>
				</div>
				<a id="moveSectionDown" href="#" class="anclas"><i class="fas fa-arrow-down"></i></a>
			</div>
		</div>
		<div class="section" id="scene07">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-4 col-md-2 col-12"></div>
					<div class="col-lg-8 col-md-10 col-12 textv-r">
						<h2>
							Etap<span>as</span>
						</h2>
						<div class="row">
							<div class="col-lg-2 col-md-1"></div>
							<div class="col-lg-8 col-md-9">
								<table class="table">
									<tr class="">
										<td class="">Inscripción <span>1.</span></td>
										<td class=""><div class="b1">Marzo</div></td>
									</tr>
									<tr class="">
										<td class="">Selección del Jurado <span>2.</span></td>
										<td class=""><div class="b2">Abril</div></td>
									</tr>
									<tr class="">
										<td class="">Publicación de Nominados <span>3.</span></td>
										<td class=""><div class="b3">Mayo</div></td>
									</tr>
									<tr class="">
										<td class="">Premiación <span>4.</span></td>
										<td class=""><div class="b4">Junio</div></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
	    	</div>
			<a id="moveSectionDown" href="#" class="anclas"><i class="fas fa-arrow-down"></i></a>
		</div>
		<div class="section" id="scene08">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 textv-r">
						<div class="row">
							<div class="col-md-8">
								<h2>
									Jura<span>do</span>
								</h2>
							</div>
							<div class="col-12"></div>
							<div class="col-md-2"></div>
							<div class="col-md-8">
								<div class="owl-carousel owl-theme">
								    <!-- <div class="item">
								    	<img src="{{asset('img/i1.png')}}" alt="">
								    	<h4>Dirección Nacional de<br>Deporte Afiliado del IPD</h4>
								    </div> -->
								    <div class="item">
								    	<!-- <img src="{{asset('img/i7.png')}}" alt=""> -->
								    	<h4>Un miembro del Consejo <br>Nacional contra la Violencia<br> en los Espectáculos<br>Deportivos </h4>
								    </div>
								    <div class="item">
								    	<!-- <img src="{{asset('img/i2.png')}}" alt=""> -->
								    	<h4>Un miembro de la Comisión<br>Nacional Antidopaje del Perú.</h4>
								    </div>
								    <div class="item">
								    	<!-- <img src="{{asset('img/i1.png')}}" alt=""> -->
								    	<h4>Un miembro del Consejo Nacional<br> para la Integración de la Persona<br> con Discapacidad. </h4>
								    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<a id="moveSectionDown" href="#" class="anclas"><i class="fas fa-arrow-down"></i></a>
	    	</div>
		</div>
		<div class="section" id="scene09">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 textv-r">
						<div class="row">
							<div class="col-md-8">
								<h2>
									Conse<span>jo</span> d<span>e</span> Hon<span>or</span>
								</h2>
							</div>
							<div class="col-12"></div>
							<div class="col-md-2"></div>
							<div class="col-md-8">
								<div class="owl-carousel owl-theme">
								    <div class="item">
								    	<!-- <img src="{{asset('img/i6.png')}}" alt=""> -->
								    	<h4>Ministerio de<br>Educación</h4>
								    </div>
								    <div class="item">
								    	<!-- <img src="{{asset('img/i4.png')}}" alt=""> -->
								    	<h4>Presidente del Instituto<br>Peruano del Deporte</h4>
								    </div>
								    <div class="item">
								    	<!-- <img src="{{asset('img/i7.png')}}" alt=""> -->
								    	<h4>Consejo Nacional contra la<br>Violencia en los Espectáculos<br>Deportivos </h4>
								    </div>
								    <div class="item">
								    	<!-- <img src="{{asset('img/i5.png')}}" alt=""> -->
								    	<h4>Presidente del Comité<br>Olímpico Peruano.</h4>
								    </div>
								    <div class="item">
								    	<!-- <img src="{{asset('img/i3.png')}}" alt=""> -->
								    	<h4>Presidente del Consejo<br>de la Prensa Peruana.</h4>
								    </div>
								</div>
							</div>
						</div>
					</div>
<!-- 					<div id="footer" class="container-fluid">
						<div class="container">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6 col-5 ">© 2018 IPD</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-7 text-r">Desarrollado por <a href="https://www.mediaimpact.pe" target="_blank">Media Impact</a></div>
							</div>
						</div>
					</div> -->
				</div>
	    	</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script src="{{asset('js/jquery.easings.min.js')}}"></script>
<script src="{{asset('js/scrolloverflow.min.js')}}"></script>
<script src="{{asset('js/jquery.fullPage.js')}}"></script>
<script src="{{asset('js/owl/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/particles.min.js')}}"></script>
<!-- <script src="{{asset('js/stats.js')}}"></script> -->
<script type="text/javascript">
	$(window).on('load', function() { // makes sure the whole site is loaded 
        $('#preload').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
     });
	$(document).ready(function() {
		$('.anclasmenu').click(function(e){               
            e.preventDefault();
            var strAncla=$(this).attr('href');
            $('body,html').stop(true,true).animate({            
                scrollTop: $(strAncla).offset().top
            },1000);

            $("body").css({"overflow": "initial"});
            $("#showmenu").css({"width":"0px"});
            $("#showmenu").css({"opacity":"0"});

        });

        $('#chanceSlider').on(' input change', function(){
				var rag = $('#chanceSlider').val();
				var text  = 15 + (rag/50);
				var h2 = 2 + (rag/110);
				var h2v = 3 + (rag/140);
				var h5 = 20 + (rag/50);
				var td = 19 + (rag/50);
				var pp = 16 + (rag/70);
				$('body, body p').css({
					'font-size': text+'px'
				});
				var hw = $(window).width();
				if(hw > 414){
				$('body h2, body h2 p').css({
					'font-size': h2+'rem'
				});
				$('#scene02 .texto-top h2').css({
					'font-size': h2+'rem'
				});
					$('.texto-v h2').css({
						'font-size': h2v+'rem'
					});
				}
				
				$('body h4').css({
					'font-size': h5+'px'
				});

				$('#scene03 p,#scene04 p, #scene05 p ,#scene06 p').css({
					'font-size': pp+'px'
				});

				$('#scene07 table tr td').css({
					'font-size': td+'px'
				});
			});

		var hh = $(window).height()+18;
		console.log(hh);
		$("#scene01").css('height',hh);
		$("#scene2").css('height',hh);
		//$("#scene03").css('height',hh);

		$(window).bind("scroll", function(){
			if ( ($(this).scrollTop() >= $("#scene03").offset().top ) && ($(this).scrollTop() <= $("#scene07").offset().top - $(this).height()) ) {
			    //Muestras al otro <div>
			    $("#fp-nav").css('display', 'block');
			}
			else{
			    //Lo ocultas
			    $("#fp-nav").css('display', 'none');
			}
		});
        
        
		$('#fullpage').fullpage({
			//anchors: ['scene00', 'scene02', 'scene03','scene06','scene07','scene08'],
			lockAnchors: false,
			//sectionsColor: ['', '#1BBC9B', '#7E8F7C'],
			navigation: true,
			slidesNavigation: true,
			navigationPosition: 'left',
			scrollBar:true,
			autoScrolling:true,
			navigationTooltips: ['Home', 'Participar', 'Disiplina','Respeto','Compañerismo','Perseverancia','Etapas'],
			//responsiveWidth: 736,

			afterLoad: function(anchorLink, index){
				$('video').each((i,v) => v.play());
			},
		});

		$('.owl-carousel').owlCarousel({
		    loop:true,
		    margin:10,
		    autoplay:true,
			smartSpeed:450,
			transitionStyle : "fade",
			//autoplayHoverPause:true,
		    navText:['<i class="fas fa-arrow-left"></i>','<i class="fas fa-arrow-right"></i>'],
		    responsiveClass:true,
		    responsive:{
		        0:{
		            items:1,
		            nav:true
		        },
		        600:{
		            items:2,
		            nav:true
		        },
		        1000:{
		            items:2,
		            nav:true,
		            loop:true
		        }
		    }
		});

		$('.anclas').click(function(e){
			e.preventDefault();
			$.fn.fullpage.moveSectionDown();
		});
		
		particlesJS('particles-js',
		  {
		    "particles": {
		      "number": {
		        "value": 80,
		        "density": {
		          "enable": true,
		          "value_area": 800
		        }
		      },
		      "color": {
		        "value": "#ffffff"
		      },
		      "shape": {
		        "type": "circle",
		        "stroke": {
		          "width": 0,
		          "color": "#000000"
		        },
		        "polygon": {
		          "nb_sides": 5
		        },
		        "image": {
		          "src": "img/github.svg",
		          "width": 100,
		          "height": 100
		        }
		      },
		      "opacity": {
		        "value": 0.5,
		        "random": false,
		        "anim": {
		          "enable": false,
		          "speed": 1,
		          "opacity_min": 0.1,
		          "sync": false
		        }
		      },
		      "size": {
		        "value": 5,
		        "random": true,
		        "anim": {
		          "enable": false,
		          "speed": 40,
		          "size_min": 0.1,
		          "sync": false
		        }
		      },
		      "line_linked": {
		        "enable": true,
		        "distance": 150,
		        "color": "#ffffff",
		        "opacity": 0.4,
		        "width": 1
		      },
		      "move": {
		        "enable": true,
		        "speed": 6,
		        "direction": "none",
		        "random": false,
		        "straight": false,
		        "out_mode": "out",
		        "attract": {
		          "enable": false,
		          "rotateX": 600,
		          "rotateY": 1200
		        }
		      }
		    },
		    "interactivity": {
		      "detect_on": "canvas",
		      "events": {
		        "onhover": {
		          "enable": true,
		          "mode": "repulse"
		        },
		        "onclick": {
		          "enable": true,
		          "mode": "push"
		        },
		        "resize": true
		      },
		      "modes": {
		        "grab": {
		          "distance": 400,
		          "line_linked": {
		            "opacity": 1
		          }
		        },
		        "bubble": {
		          "distance": 400,
		          "size": 40,
		          "duration": 2,
		          "opacity": 8,
		          "speed": 3
		        },
		        "repulse": {
		          "distance": 200
		        },
		        "push": {
		          "particles_nb": 4
		        },
		        "remove": {
		          "particles_nb": 2
		        }
		      }
		    },
		    "retina_detect": true,
		    "config_demo": {
		      "hide_card": false,
		      "background_color": "#b61924",
		      "background_image": "",
		      "background_position": "50% 50%",
		      "background_repeat": "no-repeat",
		      "background_size": "cover"
		    }
		  }
		);
		particlesJS('particles1',
		  {
		    "particles": {
		      "number": {
		        "value": 80,
		        "density": {
		          "enable": true,
		          "value_area": 800
		        }
		      },
		      "color": {
		        "value": "#ffffff"
		      },
		      "shape": {
		        "type": "circle",
		        "stroke": {
		          "width": 0,
		          "color": "#000000"
		        },
		        "polygon": {
		          "nb_sides": 5
		        },
		        "image": {
		          "src": "img/github.svg",
		          "width": 100,
		          "height": 100
		        }
		      },
		      "opacity": {
		        "value": 0.5,
		        "random": false,
		        "anim": {
		          "enable": false,
		          "speed": 1,
		          "opacity_min": 0.1,
		          "sync": false
		        }
		      },
		      "size": {
		        "value": 5,
		        "random": true,
		        "anim": {
		          "enable": false,
		          "speed": 40,
		          "size_min": 0.1,
		          "sync": false
		        }
		      },
		      "line_linked": {
		        "enable": true,
		        "distance": 150,
		        "color": "#ffffff",
		        "opacity": 0.4,
		        "width": 1
		      },
		      "move": {
		        "enable": true,
		        "speed": 6,
		        "direction": "none",
		        "random": false,
		        "straight": false,
		        "out_mode": "out",
		        "attract": {
		          "enable": false,
		          "rotateX": 600,
		          "rotateY": 1200
		        }
		      }
		    },
		    "interactivity": {
		      "detect_on": "canvas",
		      "events": {
		        "onhover": {
		          "enable": true,
		          "mode": "repulse"
		        },
		        "onclick": {
		          "enable": true,
		          "mode": "push"
		        },
		        "resize": true
		      },
		      "modes": {
		        "grab": {
		          "distance": 400,
		          "line_linked": {
		            "opacity": 1
		          }
		        },
		        "bubble": {
		          "distance": 400,
		          "size": 40,
		          "duration": 2,
		          "opacity": 8,
		          "speed": 3
		        },
		        "repulse": {
		          "distance": 200
		        },
		        "push": {
		          "particles_nb": 4
		        },
		        "remove": {
		          "particles_nb": 2
		        }
		      }
		    },
		    "retina_detect": true,
		    "config_demo": {
		      "hide_card": false,
		      "background_color": "#b61924",
		      "background_image": "",
		      "background_position": "50% 50%",
		      "background_repeat": "no-repeat",
		      "background_size": "cover"
		    }
		  }
		);
		particlesJS('particles2',
		  {
		    "particles": {
		      "number": {
		        "value": 80,
		        "density": {
		          "enable": true,
		          "value_area": 800
		        }
		      },
		      "color": {
		        "value": "#ffffff"
		      },
		      "shape": {
		        "type": "circle",
		        "stroke": {
		          "width": 0,
		          "color": "#000000"
		        },
		        "polygon": {
		          "nb_sides": 5
		        },
		        "image": {
		          "src": "img/github.svg",
		          "width": 100,
		          "height": 100
		        }
		      },
		      "opacity": {
		        "value": 0.5,
		        "random": false,
		        "anim": {
		          "enable": false,
		          "speed": 1,
		          "opacity_min": 0.1,
		          "sync": false
		        }
		      },
		      "size": {
		        "value": 5,
		        "random": true,
		        "anim": {
		          "enable": false,
		          "speed": 40,
		          "size_min": 0.1,
		          "sync": false
		        }
		      },
		      "line_linked": {
		        "enable": true,
		        "distance": 150,
		        "color": "#ffffff",
		        "opacity": 0.4,
		        "width": 1
		      },
		      "move": {
		        "enable": true,
		        "speed": 6,
		        "direction": "none",
		        "random": false,
		        "straight": false,
		        "out_mode": "out",
		        "attract": {
		          "enable": false,
		          "rotateX": 600,
		          "rotateY": 1200
		        }
		      }
		    },
		    "interactivity": {
		      "detect_on": "canvas",
		      "events": {
		        "onhover": {
		          "enable": true,
		          "mode": "repulse"
		        },
		        "onclick": {
		          "enable": true,
		          "mode": "push"
		        },
		        "resize": true
		      },
		      "modes": {
		        "grab": {
		          "distance": 400,
		          "line_linked": {
		            "opacity": 1
		          }
		        },
		        "bubble": {
		          "distance": 400,
		          "size": 40,
		          "duration": 2,
		          "opacity": 8,
		          "speed": 3
		        },
		        "repulse": {
		          "distance": 200
		        },
		        "push": {
		          "particles_nb": 4
		        },
		        "remove": {
		          "particles_nb": 2
		        }
		      }
		    },
		    "retina_detect": true,
		    "config_demo": {
		      "hide_card": false,
		      "background_color": "#b61924",
		      "background_image": "",
		      "background_position": "50% 50%",
		      "background_repeat": "no-repeat",
		      "background_size": "cover"
		    }
		  }
		);
		particlesJS('particles3',
		  {
		    "particles": {
		      "number": {
		        "value": 80,
		        "density": {
		          "enable": true,
		          "value_area": 800
		        }
		      },
		      "color": {
		        "value": "#ffffff"
		      },
		      "shape": {
		        "type": "circle",
		        "stroke": {
		          "width": 0,
		          "color": "#000000"
		        },
		        "polygon": {
		          "nb_sides": 5
		        },
		        "image": {
		          "src": "img/github.svg",
		          "width": 100,
		          "height": 100
		        }
		      },
		      "opacity": {
		        "value": 0.5,
		        "random": false,
		        "anim": {
		          "enable": false,
		          "speed": 1,
		          "opacity_min": 0.1,
		          "sync": false
		        }
		      },
		      "size": {
		        "value": 5,
		        "random": true,
		        "anim": {
		          "enable": false,
		          "speed": 40,
		          "size_min": 0.1,
		          "sync": false
		        }
		      },
		      "line_linked": {
		        "enable": true,
		        "distance": 150,
		        "color": "#ffffff",
		        "opacity": 0.4,
		        "width": 1
		      },
		      "move": {
		        "enable": true,
		        "speed": 6,
		        "direction": "none",
		        "random": false,
		        "straight": false,
		        "out_mode": "out",
		        "attract": {
		          "enable": false,
		          "rotateX": 600,
		          "rotateY": 1200
		        }
		      }
		    },
		    "interactivity": {
		      "detect_on": "canvas",
		      "events": {
		        "onhover": {
		          "enable": true,
		          "mode": "repulse"
		        },
		        "onclick": {
		          "enable": true,
		          "mode": "push"
		        },
		        "resize": true
		      },
		      "modes": {
		        "grab": {
		          "distance": 400,
		          "line_linked": {
		            "opacity": 1
		          }
		        },
		        "bubble": {
		          "distance": 400,
		          "size": 40,
		          "duration": 2,
		          "opacity": 8,
		          "speed": 3
		        },
		        "repulse": {
		          "distance": 200
		        },
		        "push": {
		          "particles_nb": 4
		        },
		        "remove": {
		          "particles_nb": 2
		        }
		      }
		    },
		    "retina_detect": true,
		    "config_demo": {
		      "hide_card": false,
		      "background_color": "#b61924",
		      "background_image": "",
		      "background_position": "50% 50%",
		      "background_repeat": "no-repeat",
		      "background_size": "cover"
		    }
		  }
		);

		var count_particles, stats, update;
		stats = new Stats;
		stats.setMode(0);
		stats.domElement.style.position = 'absolute';
		stats.domElement.style.left = '0px';
		stats.domElement.style.top = '0px';
		document.body.appendChild(stats.domElement);
		count_particles = document.querySelector('.js-count-particles');
		update = function() {
			stats.begin();
			stats.end();
			if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
				count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
			}
			requestAnimationFrame(update);
		};
		requestAnimationFrame(update);
	});

	
</script>
@endsection