@extends('layout.app')
@section('titulo','Registro')

@section('css')
<link href="{{asset('css/registro.css')}}" rel="stylesheet">
@endsection

@section('content')
<div class="container" id="pg-registro">
	<div class="row">
		<div class="col-md-2"></div>
		<div id="scene01" class="col-md-8 sceneContainer">
			<section id="registro" class="contentFrame">
				<div class="wrapper">
					<div class="row">
						<div class="col">
							<a href="{{ route('bienvenido') }}" class="atras"><i class="fas fa-angle-left"></i> Regresar a <span>Cómo participar</span></a>
						</div>
					</div>
					<div class="row">
						<div class="col box-registro">
							<div class="row">
								<div class="col text-center">
									<img src="{{asset('img/copa.png')}}" alt="">
									<img src="{{asset('img/n-copa.png')}}" alt="" class="imgn">
									<h4>Bienvenido a</h4>
									<h3>Premio Nacional a los Valores Deportivos</h3>
								</div>
							</div>
							<div class="row align-items-center">
								<div class="col-md-6">
									<form class="text-left" method="POST" action="{{ route('register') }}">
										{{ csrf_field() }}
									  <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
									    <input type="text" class="form-control" id="name" name="name" placeholder="Nombres" required>
									    @if ($errors->has('name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('name') }}</strong>
		                                    </span>
		                                @endif
									  </div>
									  <div class="form-group">
									    <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellidos" required>
									  </div>
									  <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
									    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
									    @if ($errors->has('email'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('email') }}</strong>
		                                    </span>
		                                @endif
									  </div>
									  <div class="form-group">
									    <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono">
									  </div>
									  <div class="form-group">
									    <input type="text" class="form-control" id="" name="dni" placeholder="DNI" required>
									    <input id="password" type="password" class="form-control" name="password" value="123456" style="display: none">
									    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" value="123456" style="display: none">
									  </div>
									  <button type="submit" class="btn btn-block btn-morado">Registrarme</button>
									</form>
								</div>
								<div class="col-md-6">
									<p>También puedes registrarte usando <br>otras cuentas</p>
									<a href="{{asset('/login/facebook')}}" class="btn btn-block btn-facebook">Continuar con Facebook</a>
									<a href="{{asset('/login/google')}}" class="btn btn-block btn-gmail">Continuar con Google</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection

@section('js')
<script>
	$(document).ready(function(){
		$('.anclasmenu').click(function(e){               
            var strAncla=$(this).attr('href');

            $("body").css({"overflow": "initial"});
            $("#showmenu").css({"width":"0px"});
            $("#showmenu").css({"opacity":"0"});
            location.href = './'+strAncla;

        });

        $('#chanceSlider').on(' input change', function(){
			var rag = $('#chanceSlider').val();
			var pp = 14 + (rag/70);
			var f16 = 16 + (rag/70);
			var f18 = 18 + (rag/70);
			var f20 = 20 + (rag/70);
			var f13 = 13 + (rag/70);
			var f12 = 12 + (rag/70);
			$('body').css({
				'font-size': f16+'px'
			});
			$('#scene01 #registro .wrapper .atras').css({
				'font-size': pp+'px'
			});
			$('#scene01 #registro .wrapper .atras span').css({
				'font-size': f16+'px'
			});
			$('h4').css({
				'font-size': f18+'px'
			});
			$('h3').css({
				'font-size': f20+'px'
			});
			$('.btn-facebook, .btn-gmail').css({
				'font-size': f13+'px'
			});
			$('.form-control').css({
				'font-size': f12+'px'
			});
		});
        
		var hw = $(window).width();
		var hh = $(window).height();
		if(hw > 736){
			$("#scene01").css('height',hh);
		}
		$('.anclas').click(function(e){               
            e.preventDefault();
            var strAncla=$(this).attr('href');
            $('body,html').stop(true,true).animate({                
                scrollTop: $(strAncla).offset().top
            },1000);
        });
	});
</script>
@endsection